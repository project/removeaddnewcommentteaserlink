## Contents of this file

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers


## Introduction

The "Remove links from teasers" module is a simple module that may be
used to remove the links "Add new comment","Log in to post comments"
and "Read more" from teasers.

* For a full description of the module, visit the [project page][1].

* To submit bug reports and feature suggestions, or to track changes
  visit the project's [issue tracker][2].


## Requirements

None.

## Recommended modules

* [Advanced help][4]:<br>
  When this module is enabled, display of the project's `README.md`
  will be rendered when you visit
  `help/removeaddnewcommentteaserlink/README.md`.
* [Markdown filter][5]:<br>
  When this module is enabled, display of the project's `README.md`
  will be rendered with the markdown filter.

# Installation

1. Install as you would normally install a contributed drupal
   module. See: [Installing modules][6] for further information.

2. Enable the "Remove links from teasers" module on the Modules list
   page in the administrative interface.

3. Clear all caches.


## Configuration

The module has no menu. By default, it will prevent the links "Add new
comment" and "Log in to post comments" links from appearing below
teasers. It will not remove the link "Read more".

To toggle the defaults, you may use the following *drush* commands:

    drush vset removeaddnewcommentteaserlink_comment FALSE
    drush vset removeaddnewcommentteaserlink_readmore TRUE

The first of these drush comamnds will stop removing comment links.
The second will remove the "Read more" link.

To cancel these effects and delete the variables, disable the module
and clear caches.


## Maintainers

* [free-radical](https://www.drupal.org/u/free-radical)
* [gisle](https://www.drupal.org/u/gisle)

[1]: https://drupal.org/project/removeaddnewcommentteaserlink
[2]: https://drupal.org/project/issues/removeaddnewcommentteaserlink
[3]: https://www.drupal.org/project/advanced_help_hint
[4]: https://www.drupal.org/project/advanced_help
[5]: https://www.drupal.org/project/markdown
[6]: https://drupal.org/documentation/install/modules-themes/modules-7